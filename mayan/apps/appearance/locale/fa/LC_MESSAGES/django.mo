��            )         �     �     �  P   �                    .     5     M  !   ^     �  
   �     �     �  &   �     �  
   �     �     �  '        ,     1  1   8     j  &   q     �     �  5   �  O   �     *  ]  3     �     �  I   �     �  
   �       
        $     ?  !   Z     |     �     �  
   �  E   �       #        @     P  '   j  
   �  
   �  ;   �  
   �  &   �     	     	  R   &	  O   y	     �	            
                                      	                                                                                      About Actions Be sure to change the password to increase security and to disable this message. Cancel Confirm Confirm delete Create Details for: %(object)s Edit: %(object)s Email: <strong>%(email)s</strong> First time login Identifier Insufficient permissions Login Login using the following credentials: No No results None Page not found Password: <strong>%(password)s</strong> Save Search Sorry, but the requested page could not be found. Submit Username: <strong>%(account)s</strong> Version Yes You don't have enough permissions for this operation. You have just finished installing <strong>Mayan EDMS</strong>, congratulations! required Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-07-09 06:34+0000
Last-Translator: Roberto Rosario
Language-Team: Persian (http://www.transifex.com/rosarior/mayan-edms/language/fa/)
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 درباره عملیات برای امنیت بیشتر پسورد خود را تغییر دهید لغو تائید تائید حذف ایجاد جزئیات :  %(object)s ویرایش :  %(object)s Email: <strong>%(email)s</strong> دفعه اول لاگین  مشخصه Identifier مجوز ناکافی لاگین نام کاربری و پسورد زیر را استفاده کنید خیر بی جواب و یا بی جواب هیچکدام. صفحه پیدا نشد. Password: <strong>%(password)s</strong> ذخیره جستجو متاسفانه صفحه درخواستی پیدا نشد. ارسال Username: <strong>%(account)s</strong> نسخه بلی مجوزهای لازم برای انجام این عملیات را ندارید. You have just finished installing <strong>Mayan EDMS</strong>, congratulations! الزامی 