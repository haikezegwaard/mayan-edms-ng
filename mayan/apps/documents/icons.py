from __future__ import absolute_import, unicode_literals

from appearance.classes import Icon

icon_document_preview = Icon(symbol='eye')
icon_document_properties = Icon(symbol='info')
icon_document_version_list = Icon(symbol='code-branch')
icon_document_pages = Icon(symbol='copy')
