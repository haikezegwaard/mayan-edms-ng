|pypi| |builds| |coverage| |python| |license|


Mayan EDMS NG is a modern fork of Mayan EDMS focused on stability,
perfomance and new features.

.. image:: https://gitlab.com/mayan-edms-ng/mayan-edms-ng/raw/master/docs/_static/mayan-pyramid.png
    :align: center
    :width: 200
    :height: 200

Mayan EDMS is a document management system. Its main purpose is to store,
introspect, and categorize files, with a strong emphasis on preserving the
contextual and business information of documents. It can also OCR, preview,
label, sign, send, and receive thoses files. Other features of interest
are its workflow system, role based access control, and REST API.

.. image:: https://gitlab.com/mayan-edms-ng/mayan-edms-ng/raw/master/docs/_static/overview.gif
    :align: center
    :width: 300

The easiest way to use Mayan EDMS is by using the official Docker_ image.
Make sure Docker is properly installed and working before attempting to install
Mayan EDMS.

For the complete set of installation, configuration, upgrade, and backup
instructions visit the Mayan EDMS Docker Hub page at:
https://hub.docker.com/r/mayanedmsng/mayanedmsng/

.. _Docker: https://www.docker.com/

Hardware requirements

- 2 Gigabytes of RAM (1 Gigabyte if OCR is turned off).
- Multiple core CPU (64 bit, faster than 1 GHz recommended).


Important links

- `Homepage <http://www.mayan-edms.com>`__
- `Videos <https://www.youtube.com/channel/UCJOOXHP1MJ9lVA7d8ZTlHPw>`__
- `Documentation <http://mayan.readthedocs.io/en/stable/>`__
- `Paid support <http://www.mayan-edms.com/providers/>`__
- `Roadmap <https://gitlab.com/mayan-edms-ng/mayan-edms-ng/wikis/roadmap>`__
- `Contributing <https://gitlab.com/mayan-edms-ng/mayan-edms-ng/blob/master/CONTRIBUTING.md>`__
- `Community forum <https://groups.google.com/forum/#!forum/mayan-edms>`__
- `Community forum archive <http://mayan-edms.1003.x6.nabble.com/>`__
- `Source code, issues, bugs <https://gitlab.com/mayan-edms-ng/mayan-edms-ng>`__
- `Plug-ins, other related projects <https://gitlab.com/mayan-edms-ng/>`__
- `Translations <https://www.transifex.com/rosarior/mayan-edms/>`__



.. |pypi| image:: http://img.shields.io/pypi/v/mayan-edms-ng.svg
lil   :target: http://badge.fury.io/py/mayan-edms-ng
.. |builds| image:: https://gitlab.com/mayan-edms-ng/mayan-edms-ng/badges/master/build.svg
   :target: https://gitlab.com/mayan-edms-ng/mayan-edms-ng/pipelines
.. |coverage| image:: https://codecov.io/gitlab/mayan-edms-ng/mayan-edms-ng/coverage.svg?branch=master
   :target: https://codecov.io/gitlab/mayan-edms-ng/mayan-edms-ng?branch=master
.. |python| image:: https://img.shields.io/pypi/pyversions/mayan-edms-ng.svg
.. |license| image:: https://img.shields.io/pypi/l/mayan-edms-ng.svg?style=flat
