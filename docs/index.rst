Welcome to Mayan EDMS NG!
=========================

Mayan EDMS NG is a modern fork of Mayan EDMS focused on stability,
perfomance and new features.

.. image:: /_static/mayan-pyramid.svg
  :alt: Icons made by Freepik (http://www.freepik.com) from www.flaticon.com is licensed by CC 3.0 BY (http://creativecommons.org/licenses/by/3.0/)
  :align: center
  :width: 30%


Mayan EDMS is a `Free Open Source`_ `Electronic Document Management System`_,
coded in the Python language using the Django_ web application framework and
released under the `Apache 2.0 License`_. It provides an electronic vault or
repository for electronic documents.

.. image:: /_static/overview.gif
   :alt: Overview

.. toctree::
    :hidden:

    Installation <topics/installation>
    Features <topics/features>
    Advanced deployment <topics/deploying>
    Release notes and upgrading <releases/index>
    Concepts <topics/index>
    Development <topics/development>
    App creation <topics/app_creation>
    Roadmap <topics/roadmap>
    Translations <topics/translations>
    Contributors <topics/contributors>
    Licensing <topics/license>
    FAQ <topics/faq>
    Contact <topics/contact>
    MERCs <mercs/index>
    Pending tasks <topics/pending_tasks>

.. _Docker: https://www.docker.com/
.. _Django: http://www.djangoproject.com/
.. _Free Open Source: http://en.wikipedia.org/wiki/Open_source
.. _Electronic Document Management System: https://en.wikipedia.org/wiki/Document_management_system
.. _Apache 2.0 License: https://www.apache.org/licenses/LICENSE-2.0.txt
